Automatic Weather Station

LoRaWAN based Automatic weather station is a system which measures the environmental parameters and transmits the data at regular intervals.

List of Parameters recorded

- Temperature
- Humidity
- Atmospheric Pressure
- Rain measurement
- Wind speed
- Wind Direction

LoRaWAN transmission is every 15 minutes.

The application code is written on top of the I-cube-lrwan package by STM32. 
