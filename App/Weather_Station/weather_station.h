/*
 * weather_station.h
 *
 *  Created on: Dec 1, 2020
 *      Author: akshay
 */

#ifndef CORE_INC_HW__weather_station_H_
#define CORE_INC_HW__weather_station_H_

#ifdef __cplusplus
extern "C" {
#endif


#include "BMP_180.h"
#include "bmp280.h"


#define BATT_POWER    	1
#define WINDDIR_POWER   2
#define BME280_POWER    3
#define SHT20_POWER     3
#define BMP180_POWER    3


#define BME280_INIT_SUCCESS   0
#define BME280_INIT_ERROR     1
#define BME280_READ_FAIL      1
#define BME280_READ_SUCCESS   0

#define SI1145_POWER		  3
#define SI1145_INIT_SUCCESS   0
#define SI1145_INIT_ERROR     1
#define SI1145_READ_FAIL      1
#define SI1145_READ_SUCCESS   0

typedef struct {
	float pressure_bme; /* in mbar */

	float temperature_bme; /* in �C   */

	float humidity_bme; /* in %    */

	float pressure_bmp; /* in mbar */

	float temperature_bmp; /* in �C   */

	float temperature_sht; /* in �C   */

	float humidity_sht; /* in %    */

	float UVindex ; /* UV Index
	                 0 to 2: Low
	                 3 to 5: Moderate
	                 6 to 7: High
	                 8 to 10: Very High
	                 11 or more: Extreme */

	uint16_t windDirection;

	uint16_t rainfall;

	uint16_t windSpeed;

	uint16_t batteryLevel;

} awsSensors_t;

uint16_t readBatteryLevel(void);
uint16_t getWindDirection(void);

void rainGaugeInterruptEnable();
void rainGaugeTips();
uint16_t getAccumulatedRainfall();

void windSpeedInterruptEnable();
void windSpeedRotations(void *context);
void windSpeedTimerEvent();
uint16_t getWindSpeed();
void onWindSpeedTimerEvent();
void bmp180init();
void BMP180Read(awsSensors_t *sensor_data);
void SHT20Read(awsSensors_t *sensor_data);
void sht20init();
void bme280HWInit();
bool bme280IoInit();
void readBME280(awsSensors_t *sensor_data);
//
bool SI1145Init();
void readUVIndex(awsSensors_t *sensor_data);

void weatherStationGPIO_Init();
void enable(uint8_t);
void disable(uint8_t);
void weatherStationInit();
void readWeatherStationParameters(awsSensors_t *sensor_data);

#ifdef __cplusplus
}
#endif

#endif /* CORE_INC_HW_weather_station_H_ */
